# Install & Run

This project includes all dependencies.
Please use Swift Package Manager or [accio][1] to update or reinstall the dependencies.

Run from Xcode.

# Description

With this example app I try to show my strengths in designing and implementing app architectures.
The architecture here was specificly designed by me to allow more agile development.

Some of this app's architecture properties are

* unidirectional data flow
* fully decoupled by data coupling
* UseCase based on Uncle Bob's clean architecture
* fully decoupled from UI
* app can be compiled with different UIs, which allows
    * LTS (using two different targets for different versions)
    * switching UIs for older versions (one target, switching at runtime)
    * rapid design prototyping
    * interface app with CLI to let it take a part in CI/CD
* easily testable
    * TDD & BDD are simple to do
* Messages form their own DSL

[1]: https://github.com/JamitLabs/Accio
