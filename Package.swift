// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "RaboParse",
    products: [],
    dependencies: [
        .package(url: "https://github.com/Quick/Quick.git", .upToNextMajor(from: "2.1.0")),
        .package(url: "https://github.com/Quick/Nimble.git", .upToNextMajor(from: "8.0.1")),
        .package(url: "https://github.com/yaslab/CSV.swift.git", .upToNextMajor(from: "2.4.3")),

    ],
    targets: [
        .target(
            name: "RaboParse",
            dependencies: [
                "CSV",
            ],
            path: "RaboParse"
        ),
        .testTarget(
            name: "RaboParseTests",
            dependencies: [
                "Quick",
                "Nimble"
            ]
        ),
    ]
)
