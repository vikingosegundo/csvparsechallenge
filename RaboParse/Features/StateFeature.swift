//
//  StateFeature.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

func createStateFeature(fileURL: URL,
                        fileLoader: CSVFileLoader,
                        fileWriter: CSVFileWriter,
                        output: @escaping Output) -> Input
{
    let writeCSVFile = WriteCSVFile(fileURL: fileURL, csvFileWriter: fileWriter) { response in
        switch response {
        case .failedWriting(let rows, let error): output(.state(.failed(rows, error)))
        case .writtenToFile(let rows)           : output(.state(.written(rows)))
        }
    }
        
    let loadCSVFile = LoadCSVFile(fileLoader: fileLoader) { response in
        switch response {
        case .fileLoaded   (let  rows): output(.state(.fetched(.success(rows))))
        case .loadingFailed(let error): output(.state(.fetched(.failed(error))))
        }
    }
    
    let exit: (Message) -> () = { output($0) }
    
    return { msg in
        if case .state(.write(let rows))             = msg { writeCSVFile.request(.write(rows)) }
        if case .state(.fetch)                       = msg {  loadCSVFile.request(.loadFileFrom(fileURL)) }
        if case .state(.fetched(.failed(let error))) = msg {
            if case LoadCSVFileError.fileNotFound(_) = error { exit(.appStart(.initialize)) }
        }
    }
}
