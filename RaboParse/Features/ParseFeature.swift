//
//  ParseFeature.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//
import Foundation

func createParseFeature(fileURL: URL,
                     fileLoader: CSVFileLoader,
                   userDefaults: UserDefaults,
                         output: @escaping Output) -> Input
{
    let fileLoader = LoadCSVFile(fileLoader: fileLoader) { response in
        switch response {
        case .fileLoaded   (let  rows): output(.parse(.finished(rows)))
        case .loadingFailed(let error): output(.parse(.failed( error)))
        }
    }
    
    let exit: (Message) -> () = { output($0) }

    return { request in
        if case .parse(.start)              = request { fileLoader.request(.loadFileFrom( fileURL)) }
        if case .parse(.finished(let rows)) = request { exit( .state(.write(rows)) ) }
    }
}

