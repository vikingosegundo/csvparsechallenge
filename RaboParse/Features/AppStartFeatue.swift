//
//  AppStartFeatue.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

func createAppStartFeature(fileURL: URL,
                            output: @escaping Output) -> Input
{    
    let initializer = Initializer(fileURL: fileURL) { (response) in
        switch response {
        case .wasInitialized: output(.appStart(.wasInitialized))
        }
    }
    
    let exit: (Message) -> () = { output($0) }

    return { msg in
        if case .appStart(.initialize)     = msg { initializer.request(.initialize) }
        if case .appStart(.wasInitialized) = msg { exit(.state(.fetch)) }
    }
}
