//
//  UI.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import SwiftUI

final
class UI {

    init(rootHandler: @escaping (Message) -> ()) {
        self.rootHandler = rootHandler
        contentView = ContentView(handler: rootHandler)
    }
    
    private let rootHandler: (Message) -> ()
    var contentView: ContentView

    var window: UIWindow {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UIHostingController(rootView: contentView)
        window.makeKeyAndVisible()
        return window
    }

    func handle(msg: Message) {
        contentView.handle(msg: msg)
    }
}
