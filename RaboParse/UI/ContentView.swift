//
//  ContentView.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Combine
import SwiftUI

typealias IssueRowStateHolder = StateHolder<IssueRow>

struct ContentView: View {
    
    init(handler: @escaping (Message) -> ()) {
        self.handler = handler
        self.state = IssueRowStateHolder()
    }
    
    func handle(msg: Message) {
        print("msg: \(msg)")
        if case .state(.fetched(.success(let rows))) = msg { self.state.rows = rows }
        if case .state(.fetched(.failed(let error))) = msg {
            self.state.error = error
            self.state.showingAlert = true
        }
    }
    
    var body: some View {
        var errorMessage = ""
        if let error = state.error as? LoadCSVFileError {
            switch error {
            case .fileDataCurrupted(let url):
                errorMessage = "file was currupted: \(url.lastPathComponent)"
            case .fileFormatUnknown(let url):
                errorMessage = "fileformat uknown: \(url.lastPathComponent)"
            default:
                state.showingAlert = false
            }
        }
        
        return VStack {
            List {
                ForEach(state.rows, id: \.self) { IssueRowListView(row: $0, select: { _ in }) }
                    .onDelete { self.deleteRow(at: $0) }
            }
        }.alert(isPresented: $state.showingAlert) { () -> Alert in
            Alert(title: Text("Internal error"), message: Text(errorMessage), dismissButton: .default(Text("Ok")))
        }
    }
    
    private let handler: (Message) -> ()
    @ObservedObject fileprivate var state: IssueRowStateHolder
    
    private func deleteRow(at indexSet: IndexSet) {
        for idx in indexSet {
            handler(.issue(.delete(state.rows[idx])))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        var birthday = DateComponents()
        birthday.day = 12
        birthday.month = 12
        birthday.year = 1978
        
        let cv = ContentView(handler: { _ in })
        let row = IssueRow(firstName:"Manuel", surName: "Meyer", count:42, birthday:Calendar.current.date(from: birthday)!)
        cv.handle(msg: .state(.fetched(.success([row]))))
        return cv
    }
}
