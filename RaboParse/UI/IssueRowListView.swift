//
//  IssueRowListView.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct IssueRowListView: View {
    
    let row: IssueRow
    let select: (IssueRow) -> ()
    
    var body: some View {
         VStack(alignment: .leading) {
            HStack {
                Text(row.firstName + " " + row.surName)
                    .font(.headline)
            }
            HStack {
                Text("Issue Count: \(row.count)")
                    .font(.subheadline)
                    .foregroundColor(.gray)
                    Spacer()
                Text("\(humanDF.string(for: row.birthday) ?? "")" )
                    .font(.subheadline)
                    .foregroundColor(.gray)
            }
        }
        .contentShape(Rectangle())
        .onTapGesture { self.select(self.row) }
    }
}

struct IssueRowListView_Previews: PreviewProvider {
    static var previews: some View {
        var birthday = DateComponents()
        birthday.day = 12
        birthday.month = 12
        birthday.year = 1978
        
        return IssueRowListView(
            row: IssueRow(
                firstName:"Manuel",
                surName:"Meyer",
                count: 100,
                birthday: Calendar.current.date(from: birthday)!
            ),
            select: { _ in }
        )
    }
}
