//
//  misc.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import Combine

let df: DateFormatter = {
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
    return df
}()


let humanDF: DateFormatter = {
    let df = DateFormatter()
    df.setLocalizedDateFormatFromTemplate("MMMMdyyyy")
    return df
}()


extension Sequence {
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        sorted { $0[keyPath: keyPath] < $1[keyPath: keyPath] }
    }
}


final
class StateHolder<T>: ObservableObject {
    let objectWillChange = ObservableObjectPublisher()
    var rows: [T] = []{ willSet { objectWillChange.send() } }
    
    var showingAlert = false {
        willSet { objectWillChange.send() }
        didSet  { if !showingAlert { error = nil } }
    }
    var error: Error?
}
