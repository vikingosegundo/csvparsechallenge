//
//  AllRowsAccessor.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

final
class AllRowsAccessor {
    var rows: [IssueRow] = []
    var input: Input { { if case .state(.fetched(.success(let rows))) = $0 { self.rows = rows } } }
}
