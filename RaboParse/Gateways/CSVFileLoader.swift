//
//  CSVFileLoader.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import CSV

struct CSVFileLoader {
    init(fileManager: FileManager) {
        self.fileManager = fileManager
    }
    
    private let fileManager: FileManager
    func load(url: URL) -> Result<[IssueRow], LoadCSVFileError>{
        guard
            let stream = InputStream(url: url),
            let csv = try? CSVReader(stream: stream, hasHeaderRow: true)
            else {
                return .failure(LoadCSVFileError.fileNotFound(url))
        }
        guard csv.headerRow == ["First name", "Sur name", "Issue count", "Date of birth"] else {
            return .failure(.fileFormatUnknown(url))
        }
        
        var error: LoadCSVFileError?
        let rows: [IssueRow] = csv.reduce([]) {
            guard
                $1.count == 4,
                let birthday = df.date(from: $1[3]),
                let count = Int($1[2])
            else {
                error = LoadCSVFileError.fileDataCurrupted(url)
                try? fileManager.removeItem(at: url)
                return $0
            }
            return $0 + [IssueRow(firstName: $1[0], surName: $1[1], count: count, birthday: birthday)]
        }
        return error != nil ? .failure(error!) : .success(rows)
    }
}
