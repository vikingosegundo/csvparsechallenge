//
//  IssueRowDeleter.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

final
class IssueRowDeleter {
    init(allRowsAccessor: AllRowsAccessor, output: @escaping Output) {
        self.allRowsAccessor = allRowsAccessor
        self.output = output
    }
    
    func delete(row: IssueRow) {
        output(.state(.write(allRowsAccessor.rows.filter { $0 != row })))
    }
    
    private let allRowsAccessor: AllRowsAccessor
    private let output: Output
}
