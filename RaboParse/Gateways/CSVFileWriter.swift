//
//  CSVFileWriter.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import CSV

struct CSVFileWriter {
    func write(rows: [IssueRow], to file: URL) throws {
        let fileManager = FileManager.default

        guard rows.count > 0 else { try? fileManager.removeItem(at: file); return }
        
        let stream = OutputStream(toFileAtPath: file.path, append: false)!
        let csv = try! CSVWriter(stream: stream)
        try csv.write(row: ["First name","Sur name","Issue count","Date of birth"])
        try rows.forEach { (row) in
            csv.beginNewRow()
            try csv.write(row: [row.firstName, row.surName, "\(row.count)", df.string(from: row.birthday)])
        }
        csv.stream.close()
    }
}
