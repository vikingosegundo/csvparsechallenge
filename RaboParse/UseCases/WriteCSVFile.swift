//
//  WriteCSVFile.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

struct WriteCSVFile: UseCase {
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request  { case write([IssueRow]) }
    enum Response {
        case failedWriting([IssueRow], error: Error)
        case writtenToFile([IssueRow])
    }
    
    init(fileURL: URL, csvFileWriter: CSVFileWriter, responder: @escaping (WriteCSVFile.Response) -> Void) {
        self.interactor = WriteCSVFile.Interactor(fileURL: fileURL, csvFileWriter: csvFileWriter)
        self.respond = responder
    }
    
    func request(_ request: Request) {
        switch request {
        case .write(let rows): interactor.write(rows: rows) {
            switch $0 {
            case .success(_):
                respond(.writtenToFile(rows))
            case .failure(let error):
                respond(.failedWriting(rows, error: error))
            }
            }
        }
    }
    private let respond: ((Response) -> ())
    private let interactor: Interactor
}

extension WriteCSVFile {
    final
    private class Interactor {
        init(fileURL: URL, csvFileWriter: CSVFileWriter) {
            self.fileURL = fileURL
            self.csvFileWriter = csvFileWriter
        }
        
        private let fileURL: URL
        private let csvFileWriter: CSVFileWriter
        
        func write(rows: [IssueRow], callback: (Result<Bool, Error>) -> ()) {
            do {
                try csvFileWriter.write(rows: rows, to: fileURL)
                callback(.success(true))
            } catch {
                callback(.failure(error))
            }
        }
    }
}
