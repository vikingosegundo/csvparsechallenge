//
//  DeleteIssueRow.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

struct DeleteIssueRow: UseCase {
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request  { case delete(IssueRow)  }
    enum Response { case deleted(IssueRow) }
    
    init(issueRowDeleter: IssueRowDeleter, responder: @escaping (Response) -> Void) {
        self.interactor = Interactor(issueRowDeleter: issueRowDeleter, responder: responder)
    }
    
    func request(_ request: Request) {
        switch request {
        case .delete(let row): interactor.delete(row)
        }
    }
    
    private let interactor: Interactor
}

extension DeleteIssueRow {
    final
    private class Interactor {
        init(issueRowDeleter: IssueRowDeleter, responder: @escaping (Response) -> Void) {
            self.respond = responder
            self.issueRowDeleter = issueRowDeleter

        }
        func delete(_ row: IssueRow) {
            issueRowDeleter.delete(row: row)
            respond(.deleted(row))
        }
        
        private let respond: ((Response) -> ())
        private let issueRowDeleter: IssueRowDeleter
    }
}
