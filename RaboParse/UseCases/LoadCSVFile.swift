//
//  ParseFeatureUseCases.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

enum LoadCSVFileError: Error, Equatable {
    case fileNotFound(URL)
    case fileFormatUnknown(URL)
    case fileDataCurrupted(URL)

}

struct LoadCSVFile: UseCase {
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    init(fileLoader: CSVFileLoader, responder: @escaping ((LoadCSVFile.Response) -> ())) {
        self.interactor = LoadCSVFile.Interactor(fileLoader: fileLoader) { responder($0) }
    }
    
    enum Request  { case loadFileFrom(URL) }
    enum Response {
        case fileLoaded([IssueRow])
        case loadingFailed(LoadCSVFileError)
    }
    
    func request(_ request: Request) {
        switch request {
        case .loadFileFrom(let url): interactor.parse(file: url)
        }
    }
    
    private var interactor: Interactor
}


extension LoadCSVFile {
    final
    private class Interactor {
        init(fileLoader: CSVFileLoader, responder: @escaping (LoadCSVFile.Response) -> Void) {
            self.respond = responder
            self.fileLoader = fileLoader
        }
        
        private let respond: ((Response) -> ())
        private let fileLoader: CSVFileLoader
        
        func parse(file:URL) {
            DispatchQueue.global().async {
                let respondOnMain: (Response) -> () = { res in
                    DispatchQueue.main.async {
                        self.respond(res)
                    }
                }
                switch self.fileLoader.load(url: file){
                case .success(let  rows): respondOnMain(.fileLoaded(rows))
                case .failure(let error): respondOnMain(.loadingFailed(error))
                }
            }
        }
    }
}
