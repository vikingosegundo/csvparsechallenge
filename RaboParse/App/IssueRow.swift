//
//  IssueRow.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

struct IssueRow: Equatable, Hashable {
    let firstName: String
    let surName  : String
    let count    : Int
    let birthday : Date
}
