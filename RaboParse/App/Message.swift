//
//  Message.swift
//  RaboParse
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

enum Message {
    
    case appStart(AppStart)
    case issue(Issue)
    case parse(Parse)
    case state(State)
    
    enum AppStart {
        case initialize
        case wasInitialized
    }
    
    enum Issue {
        case delete(IssueRow)
        case deleted(IssueRow)
        case deletionFailed(IssueRow)
    }
    
    enum Parse {
        case start
        case finished([IssueRow])
        case failed(LoadCSVFileError)
    }
    
    enum State {
        case write([IssueRow])
        case written([IssueRow])
        case failed([IssueRow], Error)
        
        case fetch
        case fetched(Fetched)
        
        enum Fetched {
            case success([IssueRow])
            case failed(Error)
        }
    }
}
